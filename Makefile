# This file is part of Cross Unix Documentation
# Copyright © 2019-2020 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
# SPDX-License-Identifier: CC-BY-4.0
.POSIX:

PREFIX=/usr/local
MANDIR=${PREFIX}/share/man

.PHONY: all
all:
	:

.PHONY: lint
lint:
	find ./man*/ -type f -exec man -c -Tlint {} \; | grep -v -f mandoc_lint_ignore | tee mandoc_lint.log
	wc -l <mandoc_lint.log | grep -q '^0$$'

.PHONY: install
install:
	mkdir -p ${DESTDIR}${MANDIR}/
	cp -r man[0-9]x ${DESTDIR}${MANDIR}/
