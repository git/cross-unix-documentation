# Cross-Unix Documentation
```
Copyright © 2019-2020 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
SPDX-License-Identifier: CC-BY-4.0
```
Seeing how POSIX is basically slow by design, this is an attempt to provide documentation of similarities and (noteworthy) differencies between Unix-like systems. To be used as an addition to the POSIX standard.

You can find an automatic HTML rendering of the manpages done with mandoc at: <https://hacktivis.me/git/cross-unix-documentation.mdoc/>

## Authors
Haelwenn (lanodan) Monnier <contact+c-u-d@hacktivis.me>: Gentoo Linux user and contributor, I often look at Alpine Linux, Void Linux, NetBSD, OpenBSD and 9front (non-exhaustive, non-ordered).

## Contribution
Usage of `new-manpage.sh` is recommended, to which you should change the Author section.
Also note that the format of the manpages is `mdoc(7)`, which is basically a standard nowadays as it’s used in many BSDs, some Linux distros, and GNU groff understands it.

## Copyright
Unless specified otherwise, content in this repository is under the CC-BY-4.0 license.
Also currently there is probably a lack of proper attribution to each platforms where the documentation is sourced or often copied from. I do not wish to break any kind of copyright and would be glad to discuss on any kind of propositions on how to do it properly.
The license might also change in the future to reflect that but it will stay freely redistribuable and modifiable without commercial restriction.

## Releases
For now I will not put any releases on it, but feel free to ask for a release to be made or to do it yourself.
Note: The master branch is a stable one and will not get early drafts of manpages which are to be done in separate branches to allow rebasing if needed.

## See Also
* [Rosetta Stone for Unix](http://bhami.com/rosetta.html): translation cheatsheet between Unix systems
