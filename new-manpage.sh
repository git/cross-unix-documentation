#!/bin/sh
# This file is part of Cross Unix Documentation
# Copyright © 2019-2020 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
# SPDX-License-Identifier: CC-BY-4.0
name="$2"
section="${1}x"

usage() {
	echo "${0} <section> <name>"
	echo "where section is one of [1-8]"
	exit 1
}

[[ -z "$name" ]] && usage
[[ -z "$section" ]] && usage

man_file=man${section}/${name}.${section}

cat >"${man_file}" <<EOF
.\" This file is part of Cross Unix Documentation
.\" Copyright © 2019-$(date +%Y) Haelwenn (lanodan) Monnier <contact@hacktivis.me>
.\" SPDX-License-Identifier: CC-BY-4.0
.Dd $(date +%F)
.Dt $(tr '[:lower:]' '[:upper:]' <<<"${name}") ${section}
.Os
.Sh PROLOG
This manual page is part of
.Lk https://hacktivis.me/git/cross-unix-documentation "Cross-Unix Documentation"
which is an attempt to provide documentation of similarities and (noteworthy)
differencies between Unix-like systems.
To be used as an addition to the POSIX standard.
.Sh NAME
.Nm ${name}
.Nd [short description]
.Sh SYNOPSIS
.Nm
.Sh DESCRIPTION
.Sh SOURCE
.Sh AUTHORS
.An Haelwenn (lanodan) Monnier Aq Mt contact+c-u-d@hacktivis.me
EOF

$EDITOR ${man_file}
